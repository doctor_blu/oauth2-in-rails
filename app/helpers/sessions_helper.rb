module SessionsHelper
  
  
  #-----------------------------SIGNIN E CONTROLLO CURRENT_USER----------------------------------------------------------
  def sign_in(user)
    cookies.permanent[:remember_token] = user.remember_token
    current_user = user #utente del signin = utente corrente
  end
  
   def signed_in? #metodo per controllare che l'utente corrente esista
    !current_user.nil?
  end
  
  def current_user=(user)
    @current_user = user  
  end
  
  def current_user
    @current_user ||= user_from_remember_token #la sintassi ||= viene fatta solo la prima volta, quando @current_user is undefined
  end
  
  def current_user?(user) #metodo boolean per controllare utente = current_utente
    user == current_user
  end
  
   def sign_out #metodo che si occupa del sign_out e cancella il cookies dalla sessione
    current_user = nil
    cookies.delete(:remember_token)
  end
  
  def signed_in_user #controllo login utente
      unless signed_in?
        redirect_to signin_path, :notice => "Please sign in."  #forma compatta per il flash[:notice]
      end
        
    end

  
  #--------------------------------FRIENDLY FORWARDING------------------------------------------------------------
  def redirect_back_or(default)
    redirect_to(session[:return_to] || default)
    clear_return_to
  end
  
  def store_location #salva l'URI nel cookies di sessione
    session[:return_to] = request.fullpath
  end

  def clear_return_to #cancella l'URI dal cookies
    session.delete(:return_to)
  end
  
  
  
  #---------------------------------METODI PRIVATI------------------------------------------------------------------------
  private

    def user_from_remember_token
      remember_token = cookies[:remember_token]
      User.find_by_remember_token(remember_token) unless remember_token.nil?
    end
end
