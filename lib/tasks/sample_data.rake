namespace :db do
  desc "Fill database with sample data"
  task :populate  => :environment do
    #make_users
    #make_microposts
    make_relationships
    
  end
end
    
    #---------riempie gli utenti
    def make_users
      admin = User.create!(:name => "admin",
                 :email => "admin@mail.com",
                 :password => "daniele8",
                 :password_confirmation => "daniele8")
       admin.toggle!(:admin) #setta come amministratore il primo
    
    
      99.times do |n|
        name  = Faker::Name.name
        email = "example-#{n+1}@sniffsound.com"
        password  = "password"
        User.create!(:name => name,
                     :email => email,
                     :password => password,
                     :password_confirmation => password)
      end
    end
    
    #--------- riempie i micropost
    def make_microposts
     users = User.all(:limit => 6)
      50.times do
        content = Faker::Lorem.sentence(5)
        users.each { |user| user.microposts.create!(:content => content)}
      end 
    end
    
    #-----------riempie i micropost
    def make_relationships
      users = User.all
      user = users.first
      followed_users = users[2..50]
      followers = users[3..40]
      #uno segue l'altro, passaggio a due step
      followed_users.each { |followed| user.follow!(followed)}
      followers.each { |follower| follower.follow!(user)}
    end
  