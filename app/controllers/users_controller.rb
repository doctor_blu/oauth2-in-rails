class UsersController < ApplicationController
  before_filter :signed_in_user, :only => [:edit, :update, :destroy, :index, :following, :followers]
  before_filter :correct_user,   :only => [:edit, :update]
  before_filter :admin_user,     :only => :destroy
  # filtri delle azioni che si possono fare
  
  def new
    @title = "Sign up"
    
    auth_hash = request.env["omniauth.auth"] #in autohash ho l'hash con tutte le informazioni
    
    #questo si occupa del reindirizzamento dopo la chiamata del button
    if auth_hash.nil? #
      redirect_url = '/auth/'+params['provider'];
      redirect_to redirect_url

    #controllo la sorgente della chiamata, per dividere le informazioni in base al provider  
    elsif params['provider']=='twitter'
      #gestione della città e dello split. L'hash mi restituisce [city, country]
      city = auth_hash['info']['location'] 
        if !city.nil?
         city = city.split(', ')[0]
        end
      
        @user = User.new(twitter_uid:auth_hash['uid'], 
                         name:auth_hash['info']['name'],
                         city:city)
                                       
           
    elsif params['provider']=='facebook'
        city = auth_hash['info']['location'] 
        if !city.nil?
         city = city.split(',')[0]
        end 
                
        @user = User.new(facebook_uid:auth_hash['credentials']['token'], 
                         name:auth_hash['info']['name'],
                         email:auth_hash['info']['email'],
                         city:city)
    end   
  end
  
  def show
    @user = User.find(params[:id])
  end
  
  def create
    @user = User.new(params[:user])
    if @user.save
      sign_in @user
      flash[:success] = "Ora sei autenticato con OAuth 2.0"
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def failure #metodo chiamato quando l'autenticazione fallisce
    redirect_to root_url, :alert => "Authentication error: #{params[:message].humanize}"
  end
  
  def index 
    @users = User.paginate(page: params[:page])
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User destroyed."
    redirect_to users_path
  end
  
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      #faccio il salvataggio nel DB, con update_attributes che deve di nuovo passare tutti i validatori del modello
      flash[:success] = "Profile updated"
      sign_in @user
      redirect_to @user
    else
      render 'edit'  
    end
  end
  
 
  
  #-----------------------------------metodi privati-------------------------------------------------------------------
  private
  
        
    def correct_user #controllo id utente = current_user
      @user = User.find(params[:id])
      redirect_to root_path, :notice => "Non puoi accedere al profilo di un altro" unless current_user?(@user)
    end
  
    def admin_user
      redirect_to(root_path) unless current_user.admin?
    end
  
    
end
