class ApplicationController < ActionController::Base
  protect_from_forgery #metodo usato per la sicurezza
  force_ssl #forza la sessione ad essere https
  
  include SessionsHelper
end
