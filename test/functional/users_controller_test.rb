require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end
  
  describe UsersController do
    
    render_view
    
    describe "GET 'new'" do
      it "should be successful" do
        get 'new'
        response.should be_success
      end
      
      it "should have the right title" do
        get 'new'
        response.should have_selector("title", :content => "Sign up")
      end
    end
 end

end
