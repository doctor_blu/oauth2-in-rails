class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :twitter_uid
      t.string :facebook_uid
      t.string :city

      t.timestamps
    end
  end
  
  def self.down 
    drop_table :users
  end
end
