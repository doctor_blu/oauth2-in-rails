class Authentication < ActiveRecord::Base
  attr_accessible :email, :name, :provider, :uid
  
  def self.create_with_omniauth(auth)
  create! do |authentication|
    authentication.provider = auth['provider']
    authentication.uid = auth['uid']
    if auth['info']
       authentication.name = auth['info']['name'] || ""
       authentication.email = auth['info']['email'] || ""
    end
  end
end
  
end
